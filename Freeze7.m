
classdef Freeze7 < audioPlugin
    properties (Access = public)
        hopSize = 128;
        windowSize = 1024;
        
        freezeToggle = false;
    end
    
    properties (Constant)
        PluginInterface = audioPluginInterface( ...
            audioPluginParameter('freezeToggle',...
                'DisplayName','Freeze',...
                'Style', 'vtoggle'),...
            'InputChannels',2, ...
            'OutputChannels',2, ...
            'PluginName','Spectral Freeze', ...
            'VendorName','Infinite Series Audio', ...
            'VendorVersion','1.0.0', ...
            'UniqueId','fre7', ...
            'BackgroundColor','w'); 
    end
    
    properties (Access = private)
        % window vector
        window;
        windowGain;
        
        % buffer dimensions
        numBufChans;
        bufferSize;
        numFreqBins;
        
        % buffers
        anBuf;
        synBuf;
        
        % buffer rw positions
        anBufWritePos;
        synBufReadPos;
        
        % freeze
        freezeBuffer;
        freezeCumulativePhase;
        phaseAdvance;
    end
    
    methods
        
        % constructor
        function plugin = Freeze7
            
            % as many buffer channels as hop sizes in the buffer size
            plugin.numBufChans = ceil(plugin.windowSize/plugin.hopSize);
            plugin.bufferSize = plugin.windowSize;
            plugin.numFreqBins = ceil(plugin.windowSize / 2) + 1;
            
            % analysis and synthasis buffers init
            plugin.anBuf = zeros(plugin.bufferSize, plugin.numBufChans);
            plugin.synBuf = plugin.anBuf;
            
            % window init
            plugin.window = hann(plugin.windowSize, 'periodic') / sqrt(plugin.numBufChans);
%             plugin.windowGain = plugin.getWindowGain();
            plugin.windowGain = 1;
            
            % rw positions ---- this is the golden ticket
            plugin.anBufWritePos = [1 (cumsum(ones(plugin.numBufChans-1, 1))'*plugin.hopSize)+1];
            plugin.synBufReadPos = plugin.anBufWritePos;
            
            % freeze
            plugin.freezeBuffer = zeros(plugin.bufferSize, 2);
            plugin.freezeCumulativePhase = zeros(1, plugin.numFreqBins);
            
            plugin.phaseAdvance = zeros(1, plugin.numFreqBins);
            plugin.phaseAdvance(2:plugin.numFreqBins) = ...
                (2*pi*plugin.hopSize)./(plugin.bufferSize./(1:(plugin.bufferSize/2)));
        
        end
        
        function y = spectral(plugin)
            
            % transform and window current and last buffer
            Xcurrent = fft(plugin.freezeBuffer(:, 1) .* plugin.window);
            Xlast   = fft(plugin.freezeBuffer(:, 2) .* plugin.window);
            
            % truncate
            Xcurrent = Xcurrent(1:plugin.numFreqBins);
            Xlast = Xlast(1:plugin.numFreqBins);
            
            % current magnitude
            mX = abs(Xcurrent);
            
            % phase advance
            dp = angle(Xcurrent) - angle(Xlast) - plugin.phaseAdvance';
            dp = plugin.piWrap(dp);
            
            % output spectrum
            Y = mX .* exp(1i*plugin.freezeCumulativePhase);
            
            % inverse transform
            Y = [Y; conj(flip(Y(2:end-1)))]; % hermitian symmetry
            y = real(ifft(Y));
            
            % advance cumulative phase
            plugin.freezeCumulativePhase = plugin.piWrap(plugin.freezeCumulativePhase + ...
                                plugin.phaseAdvance' + dp);
            
        end
        
        % window gain modification
        function g = getWindowGain(plugin)
            g           = [];
            
            % overlap and add windows into empty buffer
            buffer      = zeros(plugin.numBufChans*plugin.windowSize,1);
            
            for n = 1:plugin.hopSize:(length(buffer)-plugin.windowSize)
                idx         = n:(n+plugin.windowSize-1);
                buffer(idx) = buffer(idx) + plugin.window;
            end
            
            % assign central value
            g           = buffer(ceil(end/2));
        end
        
        % asynchronous buffer
        function out = process(plugin,in)
            

            % input size
            [numSamples, numChannels] = size(in);
            
            % output block
            out = zeros(numSamples, numChannels);
            
            % iterate through samples
            for n = 1:numSamples
                
                % write to buffers
                for b = 1:plugin.numBufChans
                    
                    % write to b channel
                    plugin.anBuf(plugin.anBufWritePos(b), b) = in(n);
                    
                    % if write pos is at end of buffer
                    % perform asynchronous processing
                    if plugin.anBufWritePos(b) == plugin.bufferSize
                        
                        % % % % % % % % % % % % % % % % % % % % % % % % % %
                        % check freezeToggle:
                        %
                        %   [*] if false, normal OLA and store most recent
                        %       two frames
                        %   [*] if true, spectral freeze 
                        %
                        % % % % % % % % % % % % % % % % % % % % % % % % % %
                        
                        if ~plugin.freezeToggle
                            
                            % copy to synBuf 
                            plugin.synBuf(:, b) = plugin.anBuf(:, b);

                            % apply window
                            plugin.synBuf(:, b) = power(plugin.windowGain,1) * ...
                                                    plugin.synBuf(:, b) .* plugin.window;

                            % store last freezeBuffer
                            plugin.freezeBuffer(:, 2) = plugin.freezeBuffer(:, 1);
                            
                            % update current freezeBuffer
                            plugin.freezeBuffer(:, 1) = plugin.anBuf(:, b);
                            
                            % compute and store initial phase
                            ang = angle(fft(plugin.freezeBuffer(:,1)));
                            plugin.freezeCumulativePhase = ang(1:plugin.numFreqBins);
                            
                        else
                            % process
                            plugin.synBuf(:, b) = plugin.spectral();
                        end
                        
                        
                        
                        % post-process window
                        plugin.synBuf(:, b) = power(plugin.windowGain,2) * ...
                                                plugin.synBuf(:,b) .* plugin.window;
                    end
                    
                    out(n, :) = out(n, :) + plugin.synBuf(plugin.synBufReadPos(b), b);
                end
                
                plugin.anBufWritePos = mod(plugin.anBufWritePos, ...
                                        plugin.bufferSize) + 1;
                plugin.synBufReadPos = mod(plugin.synBufReadPos, ...
                                        plugin.bufferSize) + 1;
            end
        end
    end
    
    methods (Static)
        function y = piWrap(x)
            y = x - 2*pi*floor((x+pi)/(2*pi));
        end
    end
end
